class BinarySearchTree:
    def __init__(self):
        self.size = 0  
        self._root = None  
        
    class _BSTnode:
        def __init__(self, key, value): 
            self.left = None
            self.right = None
            self.value = value  
            self.key = key  

    # Adding a node to a BST
    def add(self, key, value):
        z = self._BSTnode(key, value)
        x = self._root
        y = None
        while (x != None): 
            y = x
            if (key < x.key):
                x = x.left
            else:
                x = x.right
        if (y == None):
            self._root = z
        elif (z.key < y.key):
            y.left = z
        else:
            y.right = z
        self.size += 1
    
    # Search BST for the requested key
    def search(self, key):
        found = []
        self._search(self._root, key, found)
        return found

    def _search(self, subtree, key, found):
        if (subtree):
            if (key == subtree.key):
                found.append(1)
            elif (key < subtree.key):
                self._search(subtree.left, key, found)
            elif (key > subtree.key):
                self._search(subtree.right, key, found)

    # Find the smallest key
    def SmallestKey(self):
        nodes = []
        self._SmallestKey(self._root, nodes)
        return nodes

    def _SmallestKey(self, subtree, nodes):
        if (subtree):
            if (subtree.left == None):
                nodes.append(subtree.key)
            self._SmallestKey(subtree.left, nodes)

    # Find the largest key
    def LargestKey(self):
        nodes = []
        self._LargestKey(self._root, nodes)
        return nodes

    def _LargestKey(self, subtree, nodes):
        if (subtree):
            if (subtree.right == None):
                nodes.append(subtree.key)
            self._LargestKey(subtree.right, nodes)


    # Delete a key from a BST
    def delete(self, key):
        found = []
        self._delete(self._root, key)
        return found

    def _delete(self, subtree, key):
        if subtree is None:
            return subtree
        # If the key to be deleted is smaller than the subtree's
        if key < subtree.key:
            subtree.left = self._delete(subtree.left, key)
        # If the key to be delete is greater than the subtree's key
        elif (key > subtree.key):
            subtree.right = self._delete(subtree.right, key)
        # If key is same as subtree's key, then this is the node
        else:
            # Node with only one child or no child
            if subtree.left is None:
                temp = subtree.right
                subtree = None
                return temp
            elif subtree.right is None:
                temp = subtree.left
                subtree = None
                return temp

            # Node with two children: Get the inorder successor
            temp = self.SmallestKey(subtree.right)
            # Copy the inorder successor's content to this node
            subtree.key = temp
            # Delete the inorder successor
            subtree.right =self._delete(subtree.right, temp)

        return subtree

    # Inorder traversal
    def inorder(self):
        node=[]
        self.inorder_walk(self._root,node)
        return node

    def inorder_walk(self,subtree,node):
        if subtree:
            self.inorder_walk(subtree.left, node)
            node.append(subtree.key)
            self.inorder_walk(subtree.right,node)

    # Preorder traversal
    def preorder(self):
        node = []
        self.preorder_walk(self._root, node)
        return node

    def preorder_walk(self, subtree, node):
        if subtree:
            node.append(subtree.key)
            self.preorder_walk(subtree.left, node)
            self.preorder_walk(subtree.right, node)

    # Postorder traversal
    def postorder(self):
        node = []
        self.postorder_walk(self._root, node)
        return node

    def postorder_walk(self, subtree, node):
        if subtree:
            self.postorder_walk(subtree.left, node)
            self.postorder_walk(subtree.right, node)
            node.append(subtree.key)

if __name__=="__main__":
    B = BinarySearchTree()
    print('Adding Value to the tree')
    print('Adding 5'); B.add(5,"A value")
    print('Adding 10'); B.add(10,"A value")
    print('Adding 15'); B.add(15,"A value")
    print('Adding 20'); B.add(20,"A value")
    print('Adding 25'); B.add(25,"A value")
    print('Adding 30'); B.add(30,"A value")
    print('Adding 0'); B.add(0,"A value")
    #print("Final Size is ",str(B.size));print()

    print("Largest Key",B.LargestKey())
    print("Smallest Key",B.SmallestKey());print()

    print("Inorder Sequence : ",B.inorder())
    print("Postorder Sequence : ",B.postorder())
    print("Preorder Sequence : ",B.preorder());print()

    print("Searching for key 22")
    print("Key Found in index: ",B.search(20));print()

    print("Deleting key 2",str(B.delete(0)))
    print("Inorder after deletion",B.inorder())