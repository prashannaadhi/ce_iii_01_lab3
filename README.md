# CE_III_01_Lab3

The programs demonstrate how to implement and test binary search trees.

BinarySearchTree.py:
- Program to implement binary search tree
- Following operations accomplished:
    1. Adding a node to a BST
    2. Searching BST for the requested keys
    3. Finding the smallest key
    4. Finding the largest key
    5. Deleting a key from the BST
    6. Inorder traversal
    7. Preorder traversal
    8. Postorder traversal

Unittest.py
- Program to test each operations using test cases