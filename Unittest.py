import unittest
from BinarySearchTree import BinarySearchTree

bst = BinarySearchTree()

class BTS_TestCase(unittest.TestCase):
    def test_bstTest(self):
        # Test add function with the size
        print("Testing add() function")
        bst.add(10,"A value")
        bst.add(5, "A value")
        #self.assertListEqual(bst.size(), [2])

        # Test for inorder
        print("Testing inorder() function")
        self.assertEqual(bst.inorder(), [5,10])

        bst.add(12, "A value") # Testing in-order along with add
        self.assertEqual(bst.inorder(), [5, 10, 12])

        # Test for preorder
        print("Testing preoder() function")
        bst.add(30, "A value")  # Testing pre-order along with add
        self.assertEqual(bst.preorder(), [10,5,12,30])

        # Test for postorder
        print("Testing postorder() function")
        bst.add(8, "A value")  # Testing post-order along with add
        self.assertEqual(bst.postorder(), [8,5,30,12,10])

        # Test for Largest
        print("Testing LargestKey() function")
        self.assertEqual(bst.LargestKey(), [30])

        # Test for Smallest
        print("Testing SmallestKey() function")
        self.assertEqual(bst.SmallestKey(), [5])

        # Test for Search
        print("Testing search() function")
        self.assertEqual(bst.search(30), [1])

        # Test for Delete
        print("Testing delete() function")
        bst.delete(30) #checking Inorder after deleting
        self.assertEqual(bst.inorder(), [5,8,10,12])

if __name__=="__main__":
    unittest.main()
